from numpy import *
from pylab import *
import numpy
import matplotlib.pyplot as plt
import sys
import ntpath

if(len(sys.argv) == 4):
    # TODO: Correggere questa parte, con frequenza tipo 1234.5 si incazza
    #if((str(sys.argv[2]).isnumeric()) and (str(sys.argv[3]).isnumeric())):
        lower_freq = float(sys.argv[2])
        upper_freq = float(sys.argv[3])
        if (lower_freq>=upper_freq):
            print("Error: start at "+str(lower_freq)+" >= stop at "+str(upper_freq))
            sys.exit()
    #else:
    #    print("Invalid parameter: " +
    #          str(sys.argv[2])+" or "+str(sys.argv[3])+" number [MHz] expected!")
    #    sys.exit()
elif (len(sys.argv) == 2):
    lower_freq = float(0)
    upper_freq = float(0)
else:
    print("Invalid argument!")
    sys.exit()

file_name = sys.argv[1]

data = genfromtxt(file_name, delimiter=',')
min_intesity = numpy.min(data, axis=0)[1]

if(lower_freq <= 0.0):
    lower_freq = float(data[0, 0])/1000000.0

if(upper_freq <= 0.0):
    upper_freq = float(data[len(data)-1, 0])/1000000.0


def moving_average(a, n=3):
    ret = numpy.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def band_filter(data, lower, upper):
    row_outside_band = []

    for i in range(0, len(data)-1):
        if(data[i, 0] < lower or data[i, 0] > upper):
            row_outside_band.append(i)

    return numpy.delete(data, row_outside_band, 0)


def peak_detector(delta):
    peak = numpy.zeros(len(delta))

    salita = False
    picco = False
    ampiezza = 0
    somma = 0
    for i in range(1, len(delta)-window-1):
        if abs(delta[i]+delta[i-1]) > 0.2:
            peak[i] = delta[i]
            curr_delta = delta[i]+delta[i-1]
        else:
            peak[i] = 0

    return peak


def custom_filter(peak, data):
    salita = False
    start = 0
    discesa = 0
    stop = 0

    for i in range(0, len(peak)-1):
        if not(salita) and peak[i-1] > 0:
            salita = True
        elif salita and peak[i-1] == 0:
            discesa += 1
            if discesa == 2:
                discesa = 0
                salita = False

        if salita:
            data[i] = 0
        else:
            data[i] = data[i]

    for i in range(0, len(data)-1):
        if data[i] != 0 and data[i+1] == 0:
            start = i
        elif data[i] != 0 and data[i-1] == 0:
            avg = (data[start]+data[i])/2

            new_value = [data[start]]

            for j in range(start+1, i):
                new_value.append(avg)

            new_value = data[i]

            data[start:i+1] = new_value


for row in data:
    row[0] = row[0]/10**6
    row[1] = abs(min_intesity - row[1])

data = band_filter(data, lower_freq, upper_freq)

filtered = moving_average(data[:, 1])
delta = numpy.diff(filtered)
window = 10
soglia = 0.4
start = 0
peak_index = 0

peak = peak_detector(delta)
custom_filter(peak, filtered)

# plt.plot(data[:,0],data[:,1],'r--')
# plt.plot(data[:,0][0:-2],filtered)
# plt.plot(filtered)

plt.title('Power Spectral Density\nby IU2FRL')
plt.xlabel('Frequency (MHz)')
plt.ylabel('Signal strength (arbitrary)')
plt.grid(b=True, which='major', color='#b0aeae', linestyle='-')


plt.plot(data[:, 0][0:-2], filtered)
plt.xticks(fontsize=8)

# plt.savefig('./output/'+file_name.replace('.csv','')+'.png',dpi=1080)
plt.savefig((ntpath.basename(file_name)).replace('.csv', '')+'.png', dpi=1080)

# plt.show()
